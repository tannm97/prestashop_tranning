<?php

if (!defined('_PS_VERSION_'))
  exit;
 
class MyDemo extends Module{

  public function __construct(){
    //// module infomation
    $this->name = 'mydemo';
    $this->tab = 'administration';
    $this->version = '1.0.0';
    $this->author = 'Huy';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
    $this->bootstrap = true;
 
    parent::__construct();
 
    $this->displayName = $this->l('My module');
    $this->description = $this->l('Description of my module.');
 
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    
    //// tab infomation
    $this->tab = 'MyTab';
    $this->tabClassName = 'MyTab';
    $this->tabParentName = '';

    if (!Configuration::get('mydemo'))      
      $this->warning = $this->l('No name provided');
  }

  public function install(){
    
    /// check id tab exist
    if (!$id_tab) {
       $tab = new Tab();
       $tab->class_name = $this->tabClassName;
       $tab->id_parent = Tab::getIdFromClassName($this->tabParentName);
       $tab->module = $this->name;
       $languages = Language::getLanguages();
       foreach ($languages as $language)
         $tab->name[$language['id_lang']] = $this->displayName;
       $tab->add();
    }
    
    if (Shop::isFeatureActive())
      Shop::setContext(Shop::CONTEXT_ALL);
   
    if (!parent::install() ||
      !$this->registerHook('leftColumn') ||
      !$this->registerHook('header') ||
      !Configuration::updateValue('mydemo', 'my friend')
    )
      return false;
   
    return true;
  }

  //Allow view access to anybody
  public function viewAccess($disable = false){
         $result = true;
         return $result;
  }


  public function uninstall(){
    $id_tab = Tab::getIdFromClassName($this->tabClassName);
     if ($id_tab) {
       $tab = new Tab($id_tab);
       $tab->delete();
     }
    if (!parent::uninstall() ||
      !Configuration::deleteByName('mydemo')
    )
      return false;
   
    return true;
  }


}