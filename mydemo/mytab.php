<?php

class MyTab extends AdminController {
	public function __construct(){	
		parent::__construct();
	}
	
	public function display(){
		parent::display();
	}
	
	public function renderList() {
		// $tokenProducts = Tools::getAdminToken('MyTab'.intval(Tab::getIdFromClassName('MyTab')).intval($cookie->id_employee));
  //       var_dump($tokenProducts);die;
		return $this->context->smarty->fetch(dirname(__FILE__).'/views/templates/admin/mytemplate.tpl');
	}
	
	public function setMedia() {
        parent::setMedia();
        $this->context->controller->addJS(dirname(__FILE__).'/js/demo.js');
        $this->context->controller->addCSS(dirname(__FILE__).'/css/style.css');
    }


    //// call ajax 
	public function ajaxProcessGetProduct(){
		$result = Db::getInstance()->executeS('
					SELECT name, note FROM `'._DB_PREFIX_.'product_lang` LIMIT 5');
		die(Tools::jsonEncode($result));
	}
}